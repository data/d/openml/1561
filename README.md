# OpenML dataset: dbworld-bodies-stemmed

https://www.openml.org/d/1561

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Michele Filannino   
**Source**: UCI  
**Please cite**:   

* Dataset:  
DBworld e-mails data set
Task: dbworld-bodies-stemmed


* Source:

Michele Filannino, PhD 
University of Manchester 
Centre for Doctoral Training 
Email: filannim_AT_cs.man.ac.uk


* Data Set Information:

I collected 64 e-mails from DBWorld newsletter and I used them to train different algorithms in order to classify between 'announces of conferences' and 'everything else'. I used a binary bag-of-words representation with a stopword removal pre-processing task before.


* Attribute Information:

Each attribute corresponds to a precise word or stem in the entire data set vocabulary (I used bag-of-words representation).

* Relevant Papers:

Michele Filannino, 'DBWorld e-mail classification using a very small corpus', Project of Machine Learning course, University of Manchester, 2011.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1561) of an [OpenML dataset](https://www.openml.org/d/1561). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1561/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1561/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1561/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

